﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public struct MMKeyboardEvent
{
    public KeyCode keyCode;
    public MMKeyboardEvent(KeyCode key)
    {
        keyCode = key;
    }
    static MMKeyboardEvent e;
    public static void Trigger(KeyCode key)
    {
        e.keyCode = key;
        MMEventManager.TriggerEvent(e);
    }
}