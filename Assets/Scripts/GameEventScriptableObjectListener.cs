﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventScriptableObjectListener : MonoBehaviour
{

    public OnColorChanged _Event;
    public UnityEvent _Action;

    private void OnEnable()
    {
        _Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        _Event.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        _Action.Invoke();
    }

    public void ChangeColor()
    {
        GetComponent<Renderer>().material.color = _Event.theColor;
    }
}
