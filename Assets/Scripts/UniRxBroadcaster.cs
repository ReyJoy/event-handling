﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public struct KeyboardEvent
{
    public KeyCode keyCode { get; set; }
}

public class UniRxBroadcaster : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MessageBroker.Default.Publish(new KeyboardEvent {keyCode = KeyCode.Space});
        }
    }
}
