﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelegateBroadcaster : MonoBehaviour
{
    public delegate void OnPressSpace();
    public static event OnPressSpace OnSpacePressed;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnSpacePressed?.Invoke();
        }
    }
}
