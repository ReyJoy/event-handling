﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityEventListener : MonoBehaviour
{
    private void OnEnable()
    {
        UnityEventManager.StartListening("Spacebar Pressed", ChangeColor);
    }

    private void OnDisable()
    {
        UnityEventManager.StopListening("Spacebar Pressed", ChangeColor);
    }

    void ChangeColor()
    {
        GetComponent<Renderer>().material.color = Color.red;
    }
}
