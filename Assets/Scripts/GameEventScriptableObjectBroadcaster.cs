﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventScriptableObjectBroadcaster : MonoBehaviour
{
    public GameEventScriptableObject _Event;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _Event.Raise();
        }
    }
}
