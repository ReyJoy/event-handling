﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelegateListener : MonoBehaviour
{
    private void OnEnable()
    {
        DelegateBroadcaster.OnSpacePressed += ChangeColor;
    }

    private void OnDisable()
    {
        DelegateBroadcaster.OnSpacePressed -= ChangeColor;
    }

    void ChangeColor()
    {
        GetComponent<Renderer>().material.color = Color.red;
    }
}
