﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionBroadcaster : MonoBehaviour
{
    public static Action OnSpacePressed;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnSpacePressed?.Invoke();
        }
    }
}
