﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events/On Space Pressed")]
public class OnColorChanged : GameEventScriptableObject
{
    public Color theColor;
}
