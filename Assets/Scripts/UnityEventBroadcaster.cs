﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityEventBroadcaster : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UnityEventManager.TriggerEvent("Spacebar Pressed");
        }
    }
}
