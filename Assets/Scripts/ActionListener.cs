﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionListener : MonoBehaviour
{
    private void OnEnable()
    {
        ActionBroadcaster.OnSpacePressed += ChangeColor;
    }

    private void OnDisable()
    {
        ActionBroadcaster.OnSpacePressed -= ChangeColor;
    }

    private void ChangeColor()
    {
        GetComponent<Renderer>().material.color= Color.red;
    }
}
