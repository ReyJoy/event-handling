﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class GameEventScriptableObject : ScriptableObject
{
    private List<GameEventScriptableObjectListener> listeners = new List<GameEventScriptableObjectListener>();

    [Button(ButtonSizes.Medium)]
    public void Raise()
    {
        foreach (var listener in listeners)
        {
            listener.OnEventRaised();
        }
    }

    public void RegisterListener(GameEventScriptableObjectListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(GameEventScriptableObjectListener listener)
    {
        listeners.Remove(listener);
    }
}
