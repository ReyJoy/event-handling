﻿using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityEventManager : MonoBehaviour {

    private Dictionary <string, UnityEvent> eventDictionary;

    private static UnityEventManager unityEventManager;

    public static UnityEventManager Instance
    {
        get
        {
            if (!unityEventManager)
            {
                unityEventManager = FindObjectOfType (typeof (UnityEventManager)) as UnityEventManager;

                if (!unityEventManager)
                {
                    Debug.LogError ("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    unityEventManager.Init (); 
                }
            }

            return unityEventManager;
        }
    }

    void Init ()
    {
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, UnityEvent>();
        }
    }

    public static void StartListening (string eventName, UnityAction listener)
    {
        if (Instance.eventDictionary.TryGetValue (eventName, out var thisEvent))
        {
            thisEvent.AddListener (listener);
        } 
        else
        {
            thisEvent = new UnityEvent ();
            thisEvent.AddListener (listener);
            Instance.eventDictionary.Add (eventName, thisEvent);
        }
    }

    public static void StopListening (string eventName, UnityAction listener)
    {
        if (unityEventManager == null) return;
        if (Instance.eventDictionary.TryGetValue (eventName, out var thisEvent))
        {
            thisEvent.RemoveListener (listener);
        }
    }

    public static void TriggerEvent (string eventName)
    {
        if (Instance.eventDictionary.TryGetValue (eventName, out var thisEvent))
        {
            thisEvent.Invoke ();
        }
    }
}