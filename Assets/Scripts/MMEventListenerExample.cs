﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class MMEventListenerExample : MonoBehaviour, MMEventListener<MMKeyboardEvent>
{
    private void OnEnable()
    {
        this.MMEventStartListening();
    }

    private void OnDisable()
    {
        this.MMEventStopListening();
    }

    public void OnMMEvent(MMKeyboardEvent eventType)
    {
        switch (eventType.keyCode)
        {
            case KeyCode.Space:
                ChangeColor();
                break;
        }
    }

    void ChangeColor()
    {
        GetComponent<Renderer>().material.color = Color.red;
    }
}
