﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class UniRxListener : MonoBehaviour
{
    void OnEnable()
    {
        MessageBroker.Default.Receive<KeyboardEvent>().Subscribe(OnEvent);
    }

    void OnEvent(KeyboardEvent key)
    {
        switch (key.keyCode)
        {
            case KeyCode.Space:
                ChangeColor();
                break;
        }
    }

    private void ChangeColor()
    {
        GetComponent<Renderer>().material.color = Color.red;
    }
}
